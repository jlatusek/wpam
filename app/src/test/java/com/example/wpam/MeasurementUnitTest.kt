package com.example.wpam

import org.junit.Test
import kotlinx.serialization.json.Json
import kotlinx.serialization.encodeToString
import com.example.wpam.Measurement
import com.google.gson.Gson
import kotlinx.serialization.decodeFromString
import org.junit.Assert.*
import java.time.Instant

class MeasurementUnitTest {
    @Test
    fun serialization_test() {
        val measurement = Measurement(
            "123",
            Instant.parse("2021-11-11T18:23:21.345Z"),
            "618d57fe5fc4027d339786db",
            "618d57fe5fc4027d339786de",
            32.0,
            "1"
        )
        var gson = Gson()
        val json = gson.toJson(measurement)
        assertEquals(
            "{\"ID\":\"123\",\"timestamp\":\"2021-11-11T18:23:21.345Z\",\"sensor\":\"618d57fe5fc4027d339786db\",\"plant\":\"618d57fe5fc4027d339786de\",\"value\":32.0,\"custom_plant_id\":\"1\"}",
            json
        )
        println(json)
    }
    @Test
    fun deserialization_test() {
        val expected = Measurement(
            "123",
            Instant.parse("2021-11-11T18:23:21.345Z"),
            "618d57fe5fc4027d339786db",
            "618d57fe5fc4027d339786de",
            32.0,
            "1"
        )

        var gson = Gson()
        val result: Measurement = gson.fromJson("{\"ID\":\"123\",\"timestamp\":\"2021-11-11T18:23:21.345Z\",\"sensor\":\"618d57fe5fc4027d339786db\",\"plant\":\"618d57fe5fc4027d339786de\",\"value\":32.0,\"custom_plant_id\":\"1\"}", Measurement::class.java)
        assertEquals(expected, result)
    }

    @Test
    fun date_serialization() {
        val instant: Instant = Instant.parse("2021-11-11T18:23:21.345Z")
        println(instant)
    }
}
