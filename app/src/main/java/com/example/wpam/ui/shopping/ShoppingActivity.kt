package com.example.wpam.ui.shopping

import android.os.Bundle
import android.webkit.WebView
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.wpam.databinding.ActivityShoppingBinding

//to jest do usuniecia, bo została zastopiona przez fragment

class ShoppingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityShoppingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityShoppingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val myWebView: WebView = binding.webview
        myWebView.loadUrl("https://www.biedronka.pl/pl/kwiaty")
        }
    }

