package com.example.wpam

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface PlantMonitorService {
    @GET("api/v1/measurement")
    fun getMeasurements(@Query("sensor") sensorName: String): Call<List<Measurement>>

    @GET("api/v1/measurement/last")
    fun getLastMeasurements(@Query("sensor") sensorName: String): Call<Measurement>


    @GET("api/v1/measurement")
    fun getMeasurementsAfterDate(@Query("sensor") sensorName: String,
    @Query("afterdate") afterDate: String): Call<List<Measurement>>


    @GET("api/v1/sensor")
    fun getSensorList(): Call<List<Sensor>>

    companion object {
        var BASE_URL = "https://plant-monitor-pw.herokuapp.com/"
        fun create(): PlantMonitorService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(PlantMonitorService::class.java)
        }
    }
}
