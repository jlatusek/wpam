package com.example.wpam.ui.information

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.wpam.R

class PlantAdapter : RecyclerView.Adapter<PlantAdapter.PlantViewHolder>() {
    private var stdList: ArrayList<PlantModel> = ArrayList()
    private var onClickItem: ((PlantModel) -> Unit)? = null
    private var onClickDeleteItem: ((PlantModel) -> Unit)? = null

    fun addItems(items: ArrayList<PlantModel>){
        this.stdList = items
        notifyDataSetChanged()
    }

    fun setOnClickItem(callback: (PlantModel)->Unit){
        this.onClickItem=callback
    }

    fun setOnClickDeleteItem(callback: (PlantModel)->Unit){
        this.onClickDeleteItem=callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= PlantViewHolder (
        LayoutInflater.from(parent.context).inflate(R.layout.card_items_std,parent,false)
    )

    override fun onBindViewHolder(holder: PlantViewHolder, position: Int) {
        val std = stdList[position]
        holder.bindView(std)
        holder.itemView.setOnClickListener{onClickItem?.invoke(std)}
        holder.btnDelete.setOnClickListener{onClickDeleteItem?.invoke(std)}
    }

    override fun getItemCount(): Int {
        return stdList.size
    }


    class PlantViewHolder(var view: View): RecyclerView.ViewHolder(view){

        private var id = view.findViewById<TextView>(R.id.tvId)
        private var name = view.findViewById<TextView>(R.id.tvName)
        private var type = view.findViewById<TextView>(R.id.tvType)
        private var date = view.findViewById<TextView>(R.id.tvDate)
        private var description = view.findViewById<TextView>(R.id.tvDescription)

        var btnDelete = view.findViewById<Button>(R.id.btnDelete)

        fun bindView(std:PlantModel){
            id.text = std.id.toString()
            name.text = std.name
            type.text = std.type
            date.text = std.date
            description.text = std.description
        }
    }
}