package com.example.wpam

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.time.Instant

@RunWith(AndroidJUnit4::class)
class EventTest{
    private lateinit var eventDao: EventDao
    private lateinit var db: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java).build()
        eventDao = db.eventDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun getEmptyEventList() {
        val events: List<Event> = eventDao.getAll()
    }

    @Test
    @Throws(Exception::class)
    fun createAndGetEvent() {
        val event1 = Event(
            description = "Test description",
            timestamp = Instant.now(),
            type = EventType.FERTILIZATION
        )
        val event2 = Event(
            description = "Test description",
            timestamp = Instant.now(),
            type = EventType.IRRIGATION
        )
        eventDao.insertAll(event1)
        eventDao.insertAll(event2)
        val events: List<Event> = eventDao.getAll()
        assert(events[0] == event1)
        assert(events[1] == event2)
    }
}