package com.example.wpam

import android.util.Log
import com.example.wpam.remote.IGoogleAPIService
import com.example.wpam.remote.MapsGoogleResponse
import org.junit.Test
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class googleApiServiceTest {

    @Test
    fun nearByPlace(){
        val apiInterface = IGoogleAPIService.create().getNearbyPlaces()
        apiInterface.enqueue(object : Callback<MapsGoogleResponse> {
            override fun onResponse(
                call: Call<MapsGoogleResponse>,
                response: Response<MapsGoogleResponse>
            ) {
                println("Success")
            }
            override fun onFailure(call: Call<MapsGoogleResponse>, t: Throwable) {
                println("Error")
                t.message?.let { Log.e("TAG", it) }
            }
        })
        while(apiInterface.isExecuted) {
            println("Is executed")
        }
    }
}