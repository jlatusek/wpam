package com.example.wpam.remote;


import com.example.wpam.PlantMonitorService
import com.google.android.gms.maps.model.LatLng
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET;
import retrofit2.http.Query
import retrofit2.http.Url;

interface IGoogleAPIService {
    @GET("maps/api/place/nearbysearch/json")
    fun getNearbyPlaces(
        @Query("keyword") keyword: String,
        @Query("location") loc: String,
        @Query("type") type: String,
        @Query("radius") radius: String,
        @Query("key") key: String
    ): Call<MapsGoogleResponse>

    companion object {
        var BASE_URL = "https://maps.googleapis.com/"
        fun create(): IGoogleAPIService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(IGoogleAPIService::class.java)
        }
    }
}


data class MapsGoogleResponse(
    val html_attributions: List<Any>,
    val results: List<Result>,
    val status: String
)

data class Result(
    val business_status: String,
    val geometry: Geometry,
    val icon: String,
    val icon_background_color: String,
    val icon_mask_base_uri: String,
    val name: String,
    val opening_hours: OpeningHours,
    val permanently_closed: Boolean,
    val photos: List<Photo>,
    val place_id: String,
    val plus_code: PlusCode,
    val price_level: Int,
    val rating: Double,
    val reference: String,
    val scope: String,
    val types: List<String>,
    val user_ratings_total: Int,
    val vicinity: String
)

data class Geometry(
    val location: Location,
    val viewport: Viewport
)

data class OpeningHours(
    val open_now: Boolean
)

data class Photo(
    val height: Int,
    val html_attributions: List<String>,
    val photo_reference: String,
    val width: Int
)

data class PlusCode(
    val compound_code: String,
    val global_code: String
)

data class Location(
    val lat: Double,
    val lng: Double
)

data class Viewport(
    val northeast: Northeast,
    val southwest: Southwest
)

data class Northeast(
    val lat: Double,
    val lng: Double
)

data class Southwest(
    val lat: Double,
    val lng: Double
)