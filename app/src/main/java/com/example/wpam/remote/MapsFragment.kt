package com.example.wpam.remote

import android.content.Context
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.wpam.R

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color

import androidx.core.content.ContextCompat

import com.google.android.gms.maps.GoogleMap

import com.google.android.gms.maps.model.BitmapDescriptor
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.DrawableCompat

import androidx.core.content.res.ResourcesCompat

import androidx.annotation.ColorInt

import androidx.annotation.DrawableRes

class MapsFragment : Fragment() {

    private lateinit var mapsViewModel: MapsViewModel
    val home = LatLng(52.197983, 20.950548)

    private val callback = OnMapReadyCallback { googleMap ->
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */


        googleMap.addMarker(MarkerOptions().position(home).title("Home"))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(home))
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(home, 13.0f))

        getNearPlaces("Biedronka")
        getNearPlaces("Castorama" )
        getNearPlaces("Lidl")

        //googleMap.isMyLocationEnabled = true

        mapsViewModel.nearbyPlaces.observe(viewLifecycleOwner, Observer {
            for(i in it.results){
                println("${i.name} ${i.place_id}")
                val loc = LatLng(i.geometry.location.lat,i.geometry.location.lng)
                var isOpen = "Zamkniete"
                //if(i.opening_hours?.open_now ?: false) isOpen="Otwarte"
                var iconName = vectorToBitmap(R.drawable.ic_lidl)
                if(i.name=="Biedronka")iconName = vectorToBitmap(R.drawable.ic_biedronka)
                if(i.name=="Castorama")iconName = vectorToBitmap(R.drawable.ic_castorama)
                googleMap.addMarker(MarkerOptions()
                    .position(loc)
                    .title(i.name)
                    .snippet(isOpen)
                    .icon((iconName)))
            }
        })
    }
    fun getNearPlaces(shopName:String){
    mapsViewModel =
    ViewModelProvider(this)[MapsViewModel::class.java]
    mapsViewModel.getNearbyPlaces(shopName,home,"shop")
}

    private fun vectorToBitmap(@DrawableRes id: Int): BitmapDescriptor? {
        val vectorDrawable = ResourcesCompat.getDrawable(resources, id, null)
        val bitmap = Bitmap.createBitmap(
            vectorDrawable!!.intrinsicWidth,
            vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }


}