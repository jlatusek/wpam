package com.example.wpam.ui.information

import java.util.*

data class PlantModel(
    var id: Int = getAutoId(),
    var name: String = "",
    var type: String = "",
    var date: String = "",
    var description: String = ""
){
    companion object{
        fun getAutoId():Int{
            val random = Random()
            return random.nextInt(100)
        }
    }
}