package com.example.wpam.ui.alarms

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.wpam.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.Instant


class AlarmsViewModel : ViewModel() {
    val measurementList: MutableLiveData<List<Measurement>> = MutableLiveData()

    val lastTemperature: MutableLiveData<Measurement> = MutableLiveData()
    val lastHumidity: MutableLiveData<Measurement> = MutableLiveData()
    val lastInsolation: MutableLiveData<Measurement> = MutableLiveData()
    val lastHumidityOfSoil: MutableLiveData<Measurement> = MutableLiveData()

    val errorMessage = MutableLiveData<String>()


    fun getLastMeasurement(sensorType: SensorType) {
        val apiInterface = PlantMonitorService.create().getLastMeasurements(sensorType.sensorName)
        apiInterface.enqueue(object : Callback<Measurement> {
            override fun onResponse(
                call: Call<Measurement>,
                response: Response<Measurement>
            ) {
                when (sensorType) {
                    SensorType.HUMIDITY -> lastHumidity.postValue(response.body())
                    SensorType.TEMPERATURE -> lastTemperature.postValue(response.body())
                    SensorType.INSOLATION -> lastInsolation.postValue(response.body())
                    SensorType.HUMIDITYOFSOIL -> lastHumidityOfSoil.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<Measurement>, t: Throwable) {
                Log.e("TAG", "Get measurement error")
                t.message?.let { Log.e("TAG", it) }
                errorMessage.postValue(t.message)
            }
        })
    }
}