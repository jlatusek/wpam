package com.example.wpam.ui.chart

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.wpam.SensorType
import com.example.wpam.databinding.FragmentChartBinding
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartModel
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartType
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartView
import com.github.aachartmodel.aainfographics.aachartcreator.AASeriesElement
import com.github.aachartmodel.aainfographics.aaoptionsmodel.AAScrollablePlotArea
import java.time.ZoneId

import java.time.format.DateTimeFormatter


public open class ChartFragment : Fragment() {

    lateinit var chartViewModel: ChartViewModel
    var _binding: FragmentChartBinding? = null
    val binding get() = _binding!!
    private val temperatureModel: AAChartModel = AAChartModel()
    private val humidityModel: AAChartModel = AAChartModel()
    private val insolationModel: AAChartModel = AAChartModel()


    fun temperatureConfig() {
        chartViewModel.temperatureList.observe(viewLifecycleOwner, Observer {
            Log.i("TAG", it[0].value.toString())
            val valueList: MutableList<Double> = mutableListOf<Double>()
            val categories: MutableList<String> = mutableListOf<String>()
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
                .withZone(ZoneId.systemDefault())


            for (item in it) {
                valueList.add(item.value)
                categories.add(formatter.format(item.timestamp))
            }
            temperatureModel.series(
                arrayOf(
                    AASeriesElement()
                        .name("Temperatura")
                        .data(
                            valueList.toTypedArray()
                        ).color("#3A7A6D")
                )
            )
            temperatureModel.categories(categories.toTypedArray())
            binding.temperatureChartView.aa_drawChartWithChartModel(temperatureModel)
            binding.progressBar.visibility = View.INVISIBLE
            binding.chartsContainer.visibility = View.VISIBLE

        })
        temperatureModel.chartType(AAChartType.Area)
            .backgroundColor("#FFFFFF")
            .dataLabelsEnabled(false)
            .yAxisTitle("Temperatura")
            .scrollablePlotArea(
                AAScrollablePlotArea()
                    .minWidth(1200)
                    .scrollPositionX(1f)
            )
        binding.temperatureChartView.aa_drawChartWithChartModel(temperatureModel)

    }

    fun insolationConfig() {
        chartViewModel.insolationList.observe(viewLifecycleOwner, Observer {
            Log.i("TAG", it[0].value.toString())
            val valueList: MutableList<Double> = mutableListOf<Double>()
            val categories: MutableList<String> = mutableListOf<String>()
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
                .withZone(ZoneId.systemDefault())

            for (item in it) {
                valueList.add(item.value)
                categories.add(formatter.format(item.timestamp))
            }
            insolationModel.series(
                arrayOf(
                    AASeriesElement()
                        .name("Nasłonecznienie")
                        .data(
                            valueList.toTypedArray()
                        ).color("#3A7A6D")
                )
            )
            insolationModel.categories(categories.toTypedArray())
            binding.insolationChartView.aa_drawChartWithChartModel(insolationModel)

        })
        insolationModel.chartType(AAChartType.Area)
            .backgroundColor("#FFFFFF")
            .dataLabelsEnabled(false)
            .yAxisTitle("Nasłonecznienie")
            .scrollablePlotArea(
                AAScrollablePlotArea()
                    .minWidth(1200)
                    .scrollPositionX(1f)
            )
        binding.insolationChartView.aa_drawChartWithChartModel(insolationModel)
    }

    fun humidityConfig() {
        chartViewModel.humidityList.observe(viewLifecycleOwner, Observer {
            Log.i("TAG", it[0].value.toString())
            val valueList: MutableList<Double> = mutableListOf<Double>()
            val categories: MutableList<String> = mutableListOf<String>()
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
                .withZone(ZoneId.systemDefault())

            for (item in it) {
                valueList.add(item.value)
                categories.add(formatter.format(item.timestamp))
            }
            humidityModel.series(
                arrayOf(
                    AASeriesElement()
                        .name("Wilgotność")
                        .data(
                            valueList.toTypedArray()
                        ).color("#3A7A6D")
                )
            )
            humidityModel.categories(categories.toTypedArray())
            binding.humidityChartView.aa_drawChartWithChartModel(humidityModel)

        })
        humidityModel.chartType(AAChartType.Area)
            .backgroundColor("#FFFFFF")
            .dataLabelsEnabled(false)
            .yAxisTitle("Wilgotność")
            .scrollablePlotArea(
                AAScrollablePlotArea()
                    .minWidth(1200)
                    .scrollPositionX(1f)
            )
        binding.humidityChartView.aa_drawChartWithChartModel(humidityModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentChartBinding.inflate(inflater, container, false)
        chartViewModel =
            ViewModelProvider(this)[ChartViewModel::class.java]
        chartViewModel.getMeasurementsList(SensorType.TEMPERATURE)
        chartViewModel.getMeasurementsList(SensorType.INSOLATION)
        chartViewModel.getMeasurementsList(SensorType.HUMIDITYOFSOIL)
        chartViewModel.getMeasurementsList(SensorType.HUMIDITY)

        temperatureConfig()
        insolationConfig()
        humidityConfig()
        val view = binding.root
        // Inflate the layout for this fragment


        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

class DailyChartFragment(): ChartFragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentChartBinding.inflate(inflater, container, false)
        chartViewModel =
            ViewModelProvider(this)[ChartViewModel::class.java]
        chartViewModel.chartType = ChartType.DAILY
        chartViewModel.getMeasurementsList(SensorType.TEMPERATURE)
        chartViewModel.getMeasurementsList(SensorType.INSOLATION)
        chartViewModel.getMeasurementsList(SensorType.HUMIDITYOFSOIL)
        chartViewModel.getMeasurementsList(SensorType.HUMIDITY)

        temperatureConfig()
        insolationConfig()
        humidityConfig()
        val view = binding.root
        // Inflate the layout for this fragment
        return view
    }
}

class WeeklyChartFragment(): ChartFragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentChartBinding.inflate(inflater, container, false)
        chartViewModel =
            ViewModelProvider(this)[ChartViewModel::class.java]
        chartViewModel.chartType = ChartType.WEEKLY
        chartViewModel.getMeasurementsList(SensorType.TEMPERATURE)
        chartViewModel.getMeasurementsList(SensorType.INSOLATION)
        chartViewModel.getMeasurementsList(SensorType.HUMIDITYOFSOIL)
        chartViewModel.getMeasurementsList(SensorType.HUMIDITY)

        temperatureConfig()
        insolationConfig()
        humidityConfig()
        val view = binding.root
        // Inflate the layout for this fragment
        return view
    }
}

class MonthlyChartFragment(): ChartFragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentChartBinding.inflate(inflater, container, false)
        chartViewModel =
            ViewModelProvider(this)[ChartViewModel::class.java]
        chartViewModel.chartType = ChartType.MONTHLY
        chartViewModel.getMeasurementsList(SensorType.TEMPERATURE)
        chartViewModel.getMeasurementsList(SensorType.INSOLATION)
        chartViewModel.getMeasurementsList(SensorType.HUMIDITYOFSOIL)
        chartViewModel.getMeasurementsList(SensorType.HUMIDITY)

        temperatureConfig()
        insolationConfig()
        humidityConfig()
        val view = binding.root
        // Inflate the layout for this fragment
        return view
    }
}