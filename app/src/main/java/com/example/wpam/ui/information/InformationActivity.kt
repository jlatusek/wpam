package com.example.wpam.ui.information

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.wpam.R

class InformationActivity : AppCompatActivity() {

    // https://www.youtube.com/watch?v=9LYn-OBO5qE

    private lateinit var edName: EditText
    private lateinit var edType: EditText
    private lateinit var edDate: EditText
    private lateinit var edDescription: EditText
    private lateinit var btAdd: Button
    private lateinit var btView: Button
    private lateinit var btUpdate: Button

    private lateinit var sqliteHelper: SQLiteHelper
    private lateinit var recyclerView: RecyclerView
    private var adapter: PlantAdapter? = null
    private var std: PlantModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_information)

        initView()
        initRecyclerView()
        sqliteHelper = SQLiteHelper(this)
        btAdd.setOnClickListener { addPlant() }
        btView.setOnClickListener { getPlants() }
        btUpdate.setOnClickListener{updatePlant()}
        adapter?.setOnClickItem { Toast.makeText(this,it.name,Toast.LENGTH_SHORT).show()
            edName.setText(it.name)
            edType.setText(it.type)
            edDate.setText(it.date)
            edDescription.setText(it.description)
            std =it
        }

        adapter?.setOnClickDeleteItem {
            deletePlant(it.id)
        }

    }

        private fun getPlants() {
            val stdList = sqliteHelper.getAllPlants()
            Log.e("pppp", "${stdList.size}")

            adapter?.addItems(stdList)
        }

        private fun addPlant() {
            val name = edName.text.toString()
            val type = edType.text.toString()
            val date = edDate.text.toString()
            val description = edDescription.text.toString()

            if (name.isEmpty() || type.isEmpty()) {
                Toast.makeText(this, "Please enter requried field", Toast.LENGTH_SHORT).show()
            } else {
                val std = PlantModel(name = name, type = type, date = date, description = description)
                val status = sqliteHelper.insertPlant(std)
                if (status > -1) {
                   Toast.makeText(this, "Plant Added", Toast.LENGTH_LONG).show()
                    clearEditText()
                    getPlants()
                } else { Toast.makeText(this, "Record not saved", Toast.LENGTH_SHORT).show()
                }
            }
        }

        private fun clearEditText() {
            edName.setText("")
            edType.setText("")
            edDate.setText("")
            edDescription.setText("")
            edName.requestFocus()
        }

    private fun deletePlant(id:Int){
        if (id==null) return

        val builder = AlertDialog.Builder(this)
        builder.setMessage("Are you sure you want to delete this plant?")
        builder.setCancelable(true)
        builder.setPositiveButton("Yes"){dialog, _ ->
           sqliteHelper.deletePlantById(id)
            getPlants()
            dialog.dismiss()
        }
        builder.setNegativeButton("No"){dialog, _ ->
            dialog.dismiss()
        }

        val alert = builder.create()
        alert.show()
    }

    private fun updatePlant(){
        val name = edName.text.toString()
        val type = edType.text.toString()
        val date = edDate.text.toString()
        val description = edDescription.text.toString()

        if (name==std?.name && type == std?.type && date==std?.date && description == std?.description){
            Toast.makeText(this,"Record not changed", Toast.LENGTH_SHORT).show()
        }
        if(std==null) return
        val std = PlantModel(id=std!!.id, name = name, type = type, date = date, description = description)
        val status = sqliteHelper.updatePlant(std)
        if(status> -1){
            clearEditText()
            getPlants()
        }else{
            Toast.makeText(this,"Update filed", Toast.LENGTH_SHORT).show()
        }

    }

    private fun initRecyclerView(){
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = PlantAdapter()
        recyclerView.adapter = adapter
    }

        private fun initView() {
            edName = findViewById(R.id.edName)
            edType = findViewById(R.id.edType)
            edDate = findViewById(R.id.edDate)
            edDescription = findViewById(R.id.edDescription)
            btAdd = findViewById(R.id.btnAdd)
            btView = findViewById(R.id.btnView)
            btUpdate = findViewById(R.id.btnUpdate)
            recyclerView = findViewById(R.id.recyclerView)
        }

    }
